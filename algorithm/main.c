#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include <errno.h>
//#include "var_global.h"
//#include "read_file.h"
//#include "fase1.h"

void readFile(char problem_file[25], char dist_file[25], char time_file[25]);
void fase1();
void step0_InitSaving();
void step1_InitNewRoute();
int getK_star();
void step2_ExpandRoute();
void do_opt_2(int *kdist_opt2);
void opt_2(int *krut, int *nkrut, int *kd, int *kt, int *improve);
void opt_3(int *krut, int *nkrut, int *kd, int *kt, int *improve_tot);
void do_swap();
void swap10(int *kruta, int *nka, int *kda, int *kta, int *kaga, int *krutb, int *nkb, int *kdb, int *ktb, int *kagb, int *improve);
void swap11(int *kruta, int *nka, int *kda, int *kta, int *kaga, int *krutb, int *nkb, int *kdb, int *ktb, int *kagb, int *improve);
void insert_customer(int *krutx, int *nkx, int ix, int kpos_ix, int *kdx, int *ktx);
void feask(int *krutx, int nkx, int ktx, int ix, int *kpos_ix, int *extra_d, int *extra_t);
void remove_customer(int *krutx, int nkx, int ix, int *kruty, int *nky, int *kdy, int *kty);

void printResult();
void printResultInFile();
char* concat(const char *s1, const char *s2);

#define BOOTH_MINOR_2 200000
#define SINGLE_MINOR_2 100000
#define N_MAX 201
#define M_MAX 61
#define MAX_ROUTE 61
#define N_MAX_PER_VIAGGIO 22
#define FALSE0 0
#define TRUE1 1
#define EXTRA_T 100000
#define EXTRA_D 100000

char loc[N_MAX][20];
char outPath[50];

/*numero clienti n <= 200*/
int n = 0;
/*numero agenti si assume m<=60*/
int m; 
/*tempo massimo di lavoro di ogni agente m*/
int W; 
/*priorità*/
int pigreco[N_MAX];
/*tempo di servizio di ogni cliente*/
int delta[N_MAX]; 
/*S(0), S(1) maschera di m bit dove il bit k = 1 se l'agente k può servire il cliente i =0 altrimenti (i indice della maschera)....???????*/
long int S[N_MAX]; 

/*matrice distanze i=0..n j=0..n*/
int d[N_MAX+1][N_MAX+1];
/*matrice tempi i=0..n j=0..n*/
int t[N_MAX+1][N_MAX+1];

/*matrice saving i=1..n j=1..n NON UTILIZZARE LO ZERO*/
int sav[N_MAX+1][N_MAX+1];
/*???????????*/
long int lbit[M_MAX+1];
/*maschera degli agenti ancora disponibili*/
long int agenti; 
/* maschera degli agenti ancora disponibili per la route corrente. 
 * Prima di costruire una nuova route ag=agenti ma man mano che un cliente i viene aggiunto alla route emergente allora ag = agenti ∩ S(i)
 */
long int ag;
/*Numero di viaggi generati massimo m*/
int nroute;
int nresto;
/*deg[i] è un fleg associato ad ogni cliente i (=0 se i non è stato servito =1 altrimenti)*/
int deg[N_MAX];

/*nk(k) numero di clienti + 2 della route k=1..nroute*/
int nk[MAX_ROUTE];
/*matrice dei viaggi generati. Es. se la route k visita in sequenza i clienti 23,11,14 allora 
 *nk(k) = 5
 *route(k,1) = 0
 *route(k,2) = 23
 *route(k,3) = 11
 *route(k,4) = 14
 *route(k,5) = 0
 */
int route[M_MAX][N_MAX_PER_VIAGGIO];
/*durata(k) durata del viaggio k*/
int durata[MAX_ROUTE];
/*distanza(k) distanza del viaggio k*/
int distanza[MAX_ROUTE];
/*agente(k) agente assegnato al viaggio k*/
int agente[MAX_ROUTE];

int binaryToDecimal(int n) 
{ 
    int num = n; 
    int dec_value = 0; 
  
    // Initializing base value to 1, i.e 2^0 
    int base = 1; 
  
    int temp = num; 
    while (temp) { 
        int last_digit = temp % 10; 
        temp = temp / 10; 
  
        dec_value += last_digit * base; 
  
        base = base * 2; 
    } 
  
    return dec_value; 
}

int main(int argc, char const *argv[]) {
	int k;
	int stopFase1 = FALSE0;
	/* 
		Lettura file Problema.dat
		Lettura file Distanze.dat
		Lettura file Tempi.dat
	*/
	//readFile(argv[1], argv[2], argv[3]);
	strcpy(outPath,argv[1]);
	readFile("./files/prob.dat","./files/dist.dat","./files/time.dat");

	lbit[0] = 0;
	lbit[1] = 1;
	for (k = 2; k <= m; k++) {
		lbit[k] = lbit[k-1] + lbit[k-1];
	}

	fase1(&stopFase1);
	if (stopFase1 == TRUE1) {
		//printResult();
		//fase2();
	}
	int kdist_fase1 = 0;
	int ktime_fase1 = 0;

	//int i;
	//remove_customer(route[1],6,4,route[1],&i,&distanza[1],&durata[1]);
	//printResult();
	//route[1][4] = 1;
	//route[6][2] = 22;

	for (k = 1; k <= nroute; k++) {
		kdist_fase1 += distanza[k];
		ktime_fase1 += durata[k];
	}

	printf("\nFASE 1   :    %d\n", kdist_fase1);
	printResult();
	int kdist_opt2 = 0;
	do_opt_2(&kdist_opt2);
	printf("OPT_2    :    %d\n", kdist_opt2);

	printResult();

	do_swap();
	int kdist_swap = 0;
	int ktime_swap = 0;
	for (k = 1; k <= nroute; k++) {
		kdist_swap += distanza[k];
		ktime_swap += durata[k];
	}
	printf("SWAP     :    %d\n\n", kdist_swap);
	printResult();
	printResultInFile();

	//fase2();
	//printResult();
	return 0;
}

void readFile(char problem_file[25], char dist_file[25], char time_file[25]) {
	FILE *prob;
	FILE *dist;
	FILE *time;

	char line[240000];
	char *token;

	prob = fopen(problem_file, "r");
	dist = fopen(dist_file, "r");
	time = fopen(time_file, "r");

	/*PROBLEMA*/
	fgets(line, sizeof(line), prob);
	token = strtok(line, ";");
	n = atoi(token);
	token = strtok(NULL,";");
	m = atoi(token);
	token = strtok(NULL,";");
	W = atoi(token);

	fgets(line,sizeof(line), prob);
	token = strtok(line, ";"); 	//#cliente
	token = strtok(NULL,";");  	//latitudine
	token = strtok(NULL,";");	//longitudine
	token = strtok(NULL,";");	//località
	strcpy(loc[0],token);

	if (n > N_MAX || m > M_MAX) {
	} else {
		int i,j;
		for (i = 1; i <= n; i++) {
			fgets(line, sizeof(line), prob);
			
			token = strtok(line, ";"); 	//#cliente
			token = strtok(NULL,";");  	//latitudine
			token = strtok(NULL,";");	//longitudine
			token = strtok(NULL,";");	//località
			strcpy(loc[i],token);
			token = strtok(NULL,";");	//priorità

			//pigreco[i] = atoi(token);
			pigreco[i] = 2;
			token = strtok(NULL,";");		//tempi servizio
			delta[i] = atoi(token);
			token = strtok(NULL,";");		//capacità

			S[i] = binaryToDecimal(atoi(token));
		}
		fclose(prob);
		

		for (i = 0; i <= n; i++) {
			fgets(line, sizeof(line), dist);
			token = strtok(line, ";");
			for (j = 0; j <= n; j++) {
				d[i][j] = atoi(token);
				token = strtok(NULL, ";");
			}
		}
		fclose(dist);


		for (i = 0; i <= n; i++) {
			fgets(line, sizeof(line), time);
			token = strtok(line, ";");
			for (j = 0; j <= n; j++) {
				t[i][j] = atoi(token);
				token = strtok(NULL, ";");
			}
		}
		fclose(time);
	}
}

void fase1(int *stopFase1) {
	int callStep1 = FALSE0;
	step0_InitSaving();
	/* agenti è una word di 64 bit dove il bit k = 1 se l'agente k è libero e =0 altrimenti
	 * per inizializzare agenti si usa lbit(k) così come definito nel Program Main
	 */
	agenti = 0;
	for (int k = 1; k <= m; k++) {
		agenti = agenti | lbit[k];
	}

	/* nresto è il numero di clienti da servire
	 * nroute p il numero di route che verranno prodotte
	 * deg[i] è un fleg associato ad ognli cliente i (=0 se i non è stato servito =1 altrimenti)
	 */
	nresto = 0;
	for (int i = 1; i <= n; i++) {
		deg[i] = 0;
		nresto++;
	}
	nroute = 0;

	if (nresto == 0 || nroute == m) {
		*stopFase1 = TRUE1;
		return;//stop Fase 1
	} else {
callStepOne:
		step1_InitNewRoute(stopFase1);
		//goto step1;
		if (*stopFase1 == TRUE1) {
			return;
		} else {
			step2_ExpandRoute(&callStep1);

			if (callStep1 == TRUE1) {
				goto callStepOne;
			}
		}
	}
}

void step0_InitSaving() {

	/* calcola la matrice saving sav(i,j), i ∈ V' e j ∈ V' che sono compatibili, ovvero per cui esiste almeno un agente che li può servire.
	 * Se i e j sono incompatibili si pone sav(i,j) = -100.
	 * Nel saving si considera la priorità per cui detto ss il saving per la coppia di clienti i,j si definisce:
	 * 	- sav(i,j) = 200.000 + ss quando 𝜋(i) ≤ 2 E 𝜋(j) ≤ 2
	 *  - sav(i,j) = 100.000 + ss quando 𝜋(i) ≤ 2 E 𝜋(j) > 2 OPPURE 𝜋(i) > 2 E 𝜋(j) ≤ 2
	 *  - sav(i,j) =           ss quando 𝜋(i) > 2 E 𝜋(j) > 2         
	 */
	int ss,i,j;
	for (i = 1; i <= n; i++) {
		for (j = i; j <= n; j++) {
			if (i == j) {
				ss = -1;
				if (pigreco[i] <= 2) {
					sav[i][j] = BOOTH_MINOR_2 + ss;
				} else {
					sav[i][j] = ss;
				}
			} else if ((S[i] & S[j]) == 0) {


				sav[i][j] = -100;
			} else {
				ss = d[0][i] + d[0][j] - d[i][j];
				if (pigreco[i] <= 2 && pigreco[j] <= 2) {
					sav[i][j] = BOOTH_MINOR_2 + ss;
				} else if (pigreco[i] <= 2 || pigreco[j] <= 2) {
					sav[i][j] = SINGLE_MINOR_2 + ss;
				} else {
					sav[i][j] = ss;
				}
			}
			sav[j][i] = sav[i][j];

		}
	}
}


void step1_InitNewRoute(int *stopFase1) {
	int max_sav;
	int i_star;
	int j_star;
	int i,j;
step1:	
	max_sav = -100;
	i_star = 0;
	j_star = 0;

	*stopFase1 = FALSE0;
	for (i = 1; i <= n; i++) {
		for (j = 1; j <= n; j++) {
			if (sav[i][j] > max_sav) {
				if (deg[i] + deg[j] == 0) {
					if ((S[i] & S[j] & agenti) > 0) {
						int t_time;
						if (i == j) {
							t_time = t[0][i] + delta[i] + t[i][0];
						} else {
							t_time = t[0][i] + delta[i] + t[i][j] + delta[j] + t[j][0];
						}
						if (t_time <= W) {
							max_sav = sav[i][j];
							i_star = i;
							j_star = j;
						}
					}
				}
			}
		}
	}
	/* si assume i_star = j_star = 0 se una detta coppia non esiste. In questo caso è finita la Fase 1.
	 * Se i_star = j_star = 0 e nresto > 0 allora ciò implica che gli agenti ancora disponibili non sono compatibili
	 * con i clienti ancora non serviti.
	 * Se i_star = j_star > 0 allora il cliente i_star non può essere accoppiato con nessuno e quindi va costruita una 
	 * route che contiene solo i_star.
	 */

	if (i_star == 0 && j_star == 0) {
		*stopFase1 = TRUE1;
		return;
	} else {
		/* Inizializza il nuovo viaggio nroute+1 */
		nroute++;

		/* ag è la maschera-a-bit degli agenti che possono servire sia i_star che j_star */
		ag = S[i_star] & S[j_star] & agenti;


		if (i_star == j_star) {
			deg[i_star] = 1;
			nk[nroute] = 3;
			route[nroute][1] = 0;
			route[nroute][2] = i_star;
			route[nroute][3] = 0;
			/* Assegnare il viaggio nroute al primo agente k_star il cui bit di ag vale 1 */
			int k_star = getK_star();
			agente[nroute] = k_star;
			/* poni a 0 il bit k_star di agenti (xor)*/
			agenti = agenti^lbit[k_star];

			durata[nroute] = t[0][i_star] + delta[i_star] + t[i_star][0];
			distanza[nroute] = d[0][i_star] + d[i_star][0];

			printf("DURATA I == J   :     %d\n", durata[nroute]);

			nresto--;
			goto step1;
		} else {
			deg[i_star] = 1;
			deg[j_star] = 1;
			nk[nroute] = 4;
			route[nroute][1] = 0;
			route[nroute][2] = i_star;
			route[nroute][3] = j_star;
			route[nroute][4] = 0;
			durata[nroute] = t[0][i_star] + delta[i_star] + t[i_star][j_star] + delta[j_star] + t[j_star][0];
			distanza[nroute] = d[0][j_star] + d[i_star][j_star] + d[j_star][0];

			printf("DURATA I != J   :     %d\n", durata[nroute]);
			nresto -= 2;
			return;
		}	
	}
}


void step2_ExpandRoute(int *callStep1) {
	/* trova il cliente i_star avente saving massimo con il cliente i_primo = route[nroute][2] e il cliente j_star avente
	 * il saving massimo con il cliente j_primo = route[nroute][nk[route]-1]
	 */
	int i_primo = route[nroute][2];
	int i_star = 0;
	int i_star_sav = -100;
	int i_star_time_extra = 0;
	int i_star_dist_extra = 0;
	int i, extra, kd;

	int j_primo = route[nroute][nk[nroute]-1];
	int j_star = 0;
	int j_star_sav = -100;
	int j_star_time_extra = 0;
	int j_star_dist_extra = 0;
	int j;

	int k, k_star;
	int lt,ld,i1,i_temp,ii;

	int improve;
step2:
	
	i_primo = route[nroute][2];
	i_star = 0;
	i_star_sav = -100;
	i_star_time_extra = 0;
	i_star_dist_extra = 0;

	j_primo = route[nroute][nk[nroute]-1];
	j_star = 0;
	j_star_sav = -100;
	j_star_time_extra = 0;
	j_star_dist_extra = 0;
  	

	/* Sia i_star il cliente per cui 
	 *		sav(i_star, i_primo) = max{sav(i,i_primo) : i ∈ V, tali che deg(i) = 0 AND (S(i) & ag) > 0 AND il viaggio che
	 *                             si ottiene inserendo il cliente i prima di i_primo soddisfa i vincoli}.
	 * Si pone i_star_sav = sav(i_star, i_primo)
	 * Si assume i_star = 0 e i_star_sav = -100 se un tale cliente non esiste.
	 */
	for (i = 1; i <= n; i++) {
		if (deg[i] == 0 && (S[i] & ag) > 0) {
			extra = t[0][i] + t[i][i_primo] + delta[i] - t[0][i_primo];
			kd = durata[nroute] + extra;
			if (sav[i][i_primo] > i_star_sav && kd <= W) {
				i_star = i;
				i_star_time_extra = extra;
				i_star_dist_extra = d[0][i] + d[i][i_primo] - d[0][i_primo];
				i_star_sav = sav[i][i_primo];
			}
		}
	}


	/* Sia j_star il cliente per cui 
	 *		sav(j_star, j_primo) = max{sav(j,j_primo) : j ∈ V, tali che deg(j) = 0 AND (S(j) & ag) > 0 AND il viaggio che
	 *                             si ottiene inserendo il cliente j dopo j_primo soddisfa i vincoli}.
	 * Si pone j_star_sav = sav(j_star, j_primo)
	 * Si assume j_star = 0 e j_star_sav = -100 se un tale cliente non esiste.
	 */
	for (j = 1; j <= n; j++) {
		if (deg[j] == 0 && (S[j] & ag) > 0) {
			extra = t[0][j] + t[j][j_primo] + delta[j] - t[0][j_primo];
			kd = durata[nroute] + extra;
			if (sav[j][j_primo] > j_star_sav && kd < W) {
				j_star = j;
				j_star_time_extra = extra;
				j_star_dist_extra = d[0][j] + d[j][j_primo] - d[0][j_primo];
				j_star_sav = sav[j][j_primo];
			}
		}
	}


	/* se i_star == 0 e anche j_star == 0 allora il corrente viaggio nroute non può essere espanso ulteriormente */
	if (i_star == 0 && j_star == 0) {
		/* La corrente route non può essere espansa ulteriormente
		 * Assegna la route nroute al primo agente k_star il cui bit di ag che vale 1 (SI PUO' FARE DI MEGLIO PER SCEGLERE k_star)
		 */

		k_star = getK_star();
		agente[nroute] = k_star;
		/* porre a 0 il bit k_star di agenti (xor)*/
		agenti = agenti^lbit[k_star];
		//ricalcola la durata del viaggio nroute
		lt = 0;
		ld = 0;
		i1 = 0;
		for (k = 2; k <= nk[nroute]; k++) {
			j = route[nroute][k];
			lt += t[i1][j] + delta[j];
			ld += d[i1][j];
			i1 = j;
		} 
		durata[nroute] = lt;
		distanza[nroute] = ld;
		//go to step 1;
		*callStep1 = TRUE1;
		return;
		//step1_InitNewRoute();
	} else {
		if (i_star_sav > j_star_sav) {
			// inserisci i_star prima di i_primo = route(nroute,2)
			deg[i_star] = 1;
			i_temp = i_star;
			for (k = 2; k <= nk[nroute]; k++) {
				ii = route[nroute][k];
				route[nroute][k] = i_temp;
				i_temp = ii;
			}
			nk[nroute]++;
			route[nroute][nk[nroute]] = 0;
			durata[nroute] += i_star_time_extra;
			distanza[nroute] += i_star_dist_extra;
			ag = ag & S[i_star];
		} else {
			//inserisci j_star dopo route(nroute, nk(nroute)-1)
			deg[j_star] = 1;
			route[nroute][nk[nroute]] = j_star;
			nk[nroute]++;
			route[nroute][nk[nroute]] = 0;
			durata[nroute] += j_star_time_extra;
			distanza[nroute] += j_star_dist_extra;
			ag = ag & S[j_star];
		}

		nresto--;


		if (nk[nroute] >= 5) {
			//ottimizza il corrente viaggio nroute mediante il 2 ottimale 
			//do_opt_2();
			opt_2(route[nroute], &(nk[nroute]), &(distanza[nroute]), &(durata[nroute]), &improve);
		}
		goto step2;
	}
}

int getK_star() {
	int k_star;
	for (k_star = 0; k_star < m; k_star++) {
		if ((ag & lbit[k_star]) != 0) {
			return k_star;
		}
	}
	return FALSE0;
}

void do_opt_2(int *kdist_opt2) {
	int la,nka,k;
	int kruta[N_MAX_PER_VIAGGIO];
	int kda,kta;

	int improve;

	*kdist_opt2 = 0;

	for (la = 1; la <= nroute; la++) {
		nka = nk[la];
		for (k = 1; k <= nka; k++) {
			kruta[k] = route[la][k];
		}
		kda = distanza[la];
		kta = durata[la];

		opt_2(kruta,&nka,&kda,&kta,&improve);

		*kdist_opt2 += kda;

		if (improve > 0) {
			for (k = 1; k <= nka; k++) {
				route[la][k] = kruta[k];
			}
			distanza[la] = kda;
			durata[la] = kta;
		}
	}
}

void opt_2(int *krut, int *nkrut, int *kd, int *kt, int *improve) {
	int knew[n+1];
	int x1,x2,y1,y2;
	int k,la,lb,lk,jk;
	int ktx;
	int i0,i1;


	*improve = 0;
cento_opt:
	for (la = 2; la <= (*nkrut)-2; la++) {
		x1 = krut[la-1];
		y1 = krut[la];
		
		for (lb = la+2; lb <= *nkrut; lb++) {
			x2 = krut[lb-1];
			y2 = krut[lb];

			ktx = (*kt) - (t[x1][y1] + t[x2][y2]) + (t[x1][x2] + t[y1][y2]);

			if (d[x1][x2] + d[y1][y2] < d[x1][y1] + d[x2][y2] && ktx <= W) {
				(*improve)++;

				for (lk = 1; lk <= la-1; lk++) {
					knew[lk] = krut[lk];
				}

				jk = la - 1;
				for (lk = lb-1; lk >= la; lk--) {
					jk++;
					knew[jk] = krut[lk];
				}

				for (k = lb; k <= *nkrut; k++) {
					knew[k] = krut[k];
				}

				*kd = 0;
				*kt = 0;
				i0 = 0;
				krut[1] = 0;
				for (k = 2; k <= *nkrut; k++) {
					krut[k] = knew[k];
					i1 = krut[k];
					*kd += d[i0][i1];
					*kt += t[i0][i1] + delta[i1];
					i0 = i1;
				}
				goto cento_opt;
				
			}
		}
	}

	if (*improve > 0) {
		*kd = 0;
		*kt = 0;
		i0 = 0;
		for (k = 2; k <= *nkrut; k++) {
			i1 = krut[k];
			*kd += d[i0][i1];
			*kt += t[i0][i1] + delta[i1];
			i0 = i1;
		}
	}
}

void opt_3(int *krut, int *nkrut, int *kd, int *kt, int *improve_tot) {
	int knew[n+1];
	int x1,x2,x3;
	int y1,y2,y3;
	int kbetter;
	int i,j;
	int kdold0,ktold0;
	int n2,n1,n0;
	int m1,l11;
	int m2;
	int len1;
	int k,i0,i1;

	int improve;

	if ((*nkrut) <= 5) {
		return;
	}
	*kd = 0;
	*kt = 0;
	for (int ii = 1; ii <= (*nkrut)-1; ii++) {
		i = krut[ii];
		j = krut[ii+1];
		*kd += d[i][j];
		*kt = (*kt) + t[i][j] + delta[j];
	}
	//printf("OPT_3    :    %d\n", (*kt));
	kdold0 = *kd;
	ktold0 = *kt;

	*improve_tot = 0;
	kbetter = 0;

	n2 = (*nkrut) - 2;
	n1 = (*nkrut) - 1;
	n0 = (*nkrut);

novecento:
	*improve_tot += kbetter;
	opt_2(krut,nkrut,kd,kt,&improve);
	*improve_tot += improve;

	for (int l1 = 2; l1 <= n2; l1++) {
		y1 = krut[l1];
		x1 = krut[l1-1];

		m1 = l1 + 1;
		l11 = l1 - 1;

		for (int l2 = m1; l2 <= n1; l2++) {
			y2 = krut[l2];
			x2 = krut[l2-1];

			m2 = l2 + 1;

			for (int l3 = m2; l3 <= n0; l3++) {
				y3 = krut[l3];
				x3 = krut[l3-1];

				len1 = (*kd) - d[x1][y1] - d[x2][y2] - d[x3][y3];
				if (y1 == x2 || 
					y2 == x3 || 
					len1 + d[x1][x2] + d[y1][x3] + d[y2][y3] >= *kd) {
					goto seicento_3;
				}

				for (k = 1; k <= l11; k++) {
					knew[k] = krut[k];
				}

				k = l11;

				for (int lk = l2-1; lk >= l1; lk--) {
					k++;
					knew[k] = krut[lk];
				}

				for (int lk = l3-1; lk >= l2; lk--) {
					k++;
					knew[k] = krut[lk];
				}

				for (k = l3; k <= *nkrut; k++) {
					knew[k] = krut[k];
				}

				for (k = 1; k <= *nkrut; k++) {
					krut[k] = knew[k];
				}

				/**kd = 0;
				*kt = 0;
				i0 = 0;

				for (k = 2; k <= *nkrut; k++) {
					i1 = krut[k];
					*kd += d[i0][i1];
					*kt += t[i0][i1] + delta[i1];
					i0 = i1;
				}

				printf("1---------------------------------------------------------------------------- %d\n", (*kt));*/


				kbetter = 1;
				goto novecento;
seicento_3: 
				if (y1 == x2 || 
					y3 == x1 ||
					len1 + d[x1][x3] + d[y2][y1] + d[x2][y3] >= *kd) {
					goto settecento_3;
				}

				for (k = 1; k <= l11; k++) {
					knew[k] = krut[k];
				}

				k = l11;

				for (int lk = l3-1; lk >= l2; lk--) {
					k++;
					knew[k] = krut[lk];
				}

				for (int lk = l1; lk <= l2 - 1; lk++) {
					k++;
					knew[k] = krut[lk];
				}

				for (k = l3; k <= *nkrut; k++) {
					knew[k] = krut[k];
				}

				for (k = 1; k <= *nkrut; k++) {
					krut[k] = knew[k];
				}
				
				/**kd = 0;
				*kt = 0;
				i0 = 0;

				for (k = 2; k <= *nkrut; k++) {
					i1 = krut[k];
					*kd += d[i0][i1];
					*kt += t[i0][i1] + delta[i1];
					i0 = i1;
				}

				printf("600---------------------------------------------------------------------------- %d\n", (*kt));*/


				kbetter = 1;
				goto novecento;
settecento_3: 
				if (y2 == x3 ||
					x1 == y3 ||
					len1 + d[x1][y2] + d[x3][x2] + d[y1][y3] >= *kd) {
					goto ottocento_3;
				}

				for (k = 1; k <= l11; k++) {
					knew[k] = krut[k];
				}

				k = l11;

				for (int lk = l2; lk <= l3-1; lk++) {
					k++;
					knew[k] = krut[lk];
				}

				for (int lk = l2-1; lk >= l1; lk--) {
					k++;
					knew[k] = krut[lk];
				}

				for (k = l3; k <= *nkrut; k++) {
					knew[k] = krut[k];
				}

				for (k = 1; k <= *nkrut; k++) {
					krut[k] = knew[k];
				}


				kbetter = 1;
				goto novecento;
ottocento_3: 
				if (x1 == y3 ||
					y1 == x2 ||
					y2 == x3 ||
					len1 + d[x1][y2] + d[x3][y1] + d[x2][y3] >= *kd) {
					goto noveottanta;
				}

				for (k = 1; k <= l11; k++) {
					knew[k] = krut[k];
				}

				k = l11;

				for (int lk = l2; lk <= l3-1; lk++) {
					k++;
					knew[k] = krut[lk];
				}

				for (int lk = l1; lk <= l2-1; lk++) {
					k++;
					knew[k] = krut[lk];
				}

				for (k = l3; k <= *nkrut; k++) {
					knew[k] = krut[k];
				}

				for (k = 1; k <= *nkrut; k++) {
					krut[k] = knew[k];
				}

				kbetter = 1;
				goto novecento;
noveottanta: continue;
			}
		}
	}
	*kd = kdold0;
	*kt = ktold0;
	//printf("opt_3     ktold   :   %d\n", (*kt));

	//printf("INIZIO      :     %d\n", (*kt));

	if (*improve_tot > 0) {
		*kd = 0;
		*kt = 0;
		i0 = 0;
		for (k = 2; k <= *nkrut; k++) {
			i1 = krut[k];
			*kd += d[i0][i1];
			*kt = (*kt) + t[i0][i1] + delta[i1];
			i0 = i1;
		}
		//printf("opt_3     kt   :   %d\n", (*kt));
	}		
}

void do_swap() {

	int kruta[N_MAX_PER_VIAGGIO];
	int krutb[N_MAX_PER_VIAGGIO];
	int nka,kaga,kda,kta;
	int nkb,kagb,kdb,ktb;
	int improve_tot, improve;


	for (int la = 1; la <= nroute; la++) {
		nka = nk[la];
		kaga = agente[la];
		
		for (int k = 1; k <= nka; k++) {
			kruta[k] = route[la][k];
		}
		kda = distanza[la];
		kta = durata[la];

		for (int lb = 1; lb <= nroute; lb++) {
			if (la == lb) {
				goto cinquecento_swap;
			}
			nkb = nk[lb];
			kagb = agente[lb];
			for (int k = 1; k <= nkb; k++) {
				krutb[k] = route[lb][k];
			}
			kdb = distanza[lb];
			ktb = durata[lb];

			improve_tot = 0;
			int conta10 = 0;
			int conta11 = 0;

cento_swap:		
			//printf("la,lb : %d,%d\n",la,lb);
			//printf("10 : %d\n",conta10);
			//printf("11 : %d\n",conta11);
			swap10(kruta,&nka,&kda,&kta,&kaga,
				   krutb,&nkb,&kdb,&ktb,&kagb, &improve);
			conta10++;
			improve_tot += improve;
			
			if (improve > 0) {
				//printf("10 --->   %d\n",improve );
				goto cento_swap;
			}

			swap11(kruta,&nka,&kda,&kta,&kaga,
				   krutb,&nkb,&kdb,&ktb,&kagb, &improve);
			improve_tot += improve;
			conta11++;
			

			if (improve > 0) {
				//printf("11 --->   %d\n",improve );
				goto cento_swap;
			}
			//printf("improve_tot : %d\n", improve_tot); 
			if (improve_tot > 0) {
				nk[la] = nka;
				for (int k = 1; k <= nka; k++) {
					route[la][k] = kruta[k];
				}
				distanza[la] = kda;
				durata[la] = kta;
				printf("do_swap definitivo  durata --> kta   :   %d\n", durata[la]);

				nk[lb] = nkb;
				for (int k = 1; k <= nkb; k++) {
					route[lb][k] = krutb[k];
				}
				distanza[lb] = kdb;
				durata[lb] = ktb;
				printf("do_swap definitivo  durata --> ktb   :   %d\n", durata[lb]);
			}
cinquecento_swap:
			continue;
		}
	}
}

void swap10(int *kruta, int *nka, int *kda, int *kta, int *kaga, int *krutb, int *nkb, int *kdb, int *ktb, int *kagb, int *improve) {
	/* swap10 riceve in input 
	 * kruta route di cardinalità nka, lunghezza kda e durata kta e agente kaga
	 * krutb route di cardinalità nkb, lunghezza kdb e durata ktb e agente kagb
	 *
	 * Cerca di inserire uno dei clienti di kruta in krutb
	 * output : improve >= 1 se vi è stato miglioramento; = 0 altrimenti
	 */
	int ia,isavd,isavt;
	int extrad,extrat;
	int kexd,kext;
	int ind;
	int ka;

	int kflag;
	*improve = 0;

	if (*nka <= 2 || *nkb <= 2) {
		goto cinquecento10;
	}
cinquanta10:

    for (ka = 2; ka <= (*nka)-1; ka++) {
		ia = kruta[ka];
		//TODO fra
		if ((S[ia] & lbit[*kagb]) == 0 || *ktb + delta[ia] > W) {
			goto duecinquanta10;
		}
		isavd = d[kruta[ka-1]][ia] + d[ia][kruta[ka+1]] - d[kruta[ka-1]][kruta[ka+1]];
		isavt = t[kruta[ka-1]][ia] + t[ia][kruta[ka+1]] - t[kruta[ka-1]][kruta[ka+1]] + delta[ia];
		//trova la posizione migliore di krutb dove inserire il cliente i
		extrad = isavd;
		extrat = isavt;
		//printf("swap10    isavt   :   %d\n", isavt);
		ind = 0;

		for (int kb = 1; kb <= (*nkb)-1; kb++) {
			kexd = d[krutb[kb]][ia] + d[ia][krutb[kb+1]] - d[krutb[kb]][krutb[kb+1]];
			kext = t[krutb[kb]][ia] + t[ia][krutb[kb+1]] - t[krutb[kb]][krutb[kb+1]] + delta[ia];
			//printf("swap10    kext   :   %d\n", kext);
			if ((*ktb) + kext <= W && kexd < extrad) {
				extrad = kexd;
				extrat = kext;
				//printf("swap10    extrat   :   %d\n", extrat);
				ind = kb;
			}
		}
		//se ind > 0 allora conviene inserire ia nella krutb dopo la posizione ind
		if (ind > 0) {
			goto trecento;
		}
duecinquanta10: continue;
	}
	goto cinquecento10;	
trecento:
	(*improve)++;
	//rimuovi ia dalla posizione ka di kruta
	(*kta) -= isavt;
	(*kda) -= isavd;
	//printf("rimuovi ia dalla posizione ka di kruta   :    %d\n", (*kta));


	for (int k = ka; k <= (*nka)-1; k++) {
		kruta[k] = kruta[k+1];
	}

	(*nka)--;

	//inserisci ia nella route krutb dopo la posizione ind
	*ktb += extrat;
	*kdb += extrad;
	//printf("inserisci ia nella route krutb dopo la posizione ind    :    %d\n", (*ktb));
	int ix = ia;
	for (int k = ind+1; k <= *nkb; k++) {
		int iy = krutb[k];
		krutb[k] = ix;
		ix = iy;
	}
	(*nkb)++;
	krutb[*nkb] = 0;

	if (*nka >= 5) {
		opt_2(kruta, nka, kda, kta, &kflag);
		opt_3(kruta, nka, kda, kta, &kflag);
	}
	if (*nkb >= 5) {
		opt_2(krutb, nkb, kdb, ktb, &kflag);
		opt_3(krutb, nkb, kdb, ktb, &kflag);
	}

	if (*nka > 2 && *nkb > 2) {
		goto cinquanta10;
	}
cinquecento10:
	return;
}

void swap11(int *kruta, int *nka, int *kda, int *kta, int *kaga, int *krutb, int *nkb, int *kdb, int *ktb, int *kagb, int *improve) {
	int krutax[N_MAX_PER_VIAGGIO];
	int krutbx[N_MAX_PER_VIAGGIO];
	int ia,ib;
	int kdax, ktax;
	int kdbx, ktbx;	
	int nkax = 0;
	int nkbx = 0;
	int kpos_ib = 0;
	int kpos_ia = 0;
	int kdextrax,ktextrax;
	int kdextrbx,ktextrbx;

	int kflag;
	/* Swap11 riceve in input gli stessi dati di Swap10
	 * kruta route di cardinalità nka, lunghezza kda e durata kta e agente kaga
	 * krutb route di cardinalità nkb, lunghezza kdb e durata ktb e agente kagb
	 * 
	 * Swap11 cerca di scambiare un cliente di kruta con un cliente di krutb
	 */
	*improve = 0;
	if (*nka <= 2 || *nkb <= 2) {
		goto cinquecento11;
	}
cinquanta11:
	for (int ka = 2; ka <= (*nka)-1; ka++) {
		ia = kruta[ka];
		if ((S[ia] & lbit[*kagb]) == 0 /*|| ktb + delta[ia] > W*/) {
			goto duecinquanta11;
		}
		/* rimuovi ia da kruta e chiama krutax la route risultante
		 * kdax e ktax sono distanza e tempo di krutax
		 */

		remove_customer(kruta,*nka,ia,krutax,&nkax,&kdax,&ktax);
		for (int kb = 2; kb <= (*nkb)-1; kb++) {
			ib = krutb[kb];
			if ((S[ib] & lbit[*kaga]) == 0 /*|| ktb + delta[ib] > W*/) {
				goto unocinquanta11;
			}
			feask(krutax, nkax, ktax, ib, &kpos_ib, &kdextrax, &ktextrax);
			/* kdextrax e ktextrax sono distanza e tempo aggiuntivi
			 * a kdax e ktax per inserire ib in krutax in posizione kpos_ib;
			 */

			if (kpos_ib == -1) {
				goto unocinquanta11;
			}

			/* rimuovi ib da krutb e chiama krutbx la route risultante
		     * kdbx e ktbx sono distanza e tempo di krutbx
			 */

			remove_customer(krutb,*nkb,ib,krutbx,&nkbx,&kdbx,&ktbx);

			//trova la migliore posizione kpos_ia per inserire in in krutbx

			feask(krutbx, nkbx, ktbx, ia, &kpos_ia, &kdextrbx, &ktextrbx);
			/* kdextrbx e ktextrbx sono distanza e tempo aggiuntivi
			 * a kdbx e ktbx per inserire ia in krutbx in posizione kpos_ia;
			 */
			if (kpos_ia == -1) {
				goto unocinquanta11;
			}

			/* vedi se conviene fare lo scambio */

			if (kdax + kdextrax + kdbx + kdextrbx < (*kda) + (*kdb)) {
				/* se si arriva qui allora lo scambio è conveniente, per cui
				 * si inserisce ib in krutax e ia in krutbx
				 */
				insert_customer(krutax,&nkax,ib,kpos_ib,&kdax,&ktax);
				insert_customer(krutbx,&nkbx,ia,kpos_ia,&kdbx,&ktbx);

				(*improve)++;

				/* copia krutax in kruta e krutbx in krutb*/
				for (int k = 1; k <= nkax; k++) {
					kruta[k] = krutax[k];
				}
				*kda = kdax;
				*kta = ktax;

				for (int k = 1; k <= nkbx; k++) {
					krutb[k] = krutbx[k];
				}
				*kdb = kdbx;
				*ktb = ktbx;

				if ((*nka) >= 5) {
					opt_2(kruta, nka, kda, kta, &kflag);
					opt_3(kruta, nka, kda, kta, &kflag);
				}
				if (*nkb >= 5) {
					opt_2(krutb, nkb, kdb, ktb, &kflag);
					opt_3(krutb, nkb, kdb, ktb, &kflag);
				}
				goto cinquanta11; 
			}
unocinquanta11: continue;
		}
duecinquanta11: continue;
	}
cinquecento11:
	return;

}

void insert_customer(int *krutx, int *nkx, int ix, int kpos_ix, int *kdx, int *ktx) {
	int i0 = ix;
	int iy;
	delta[0] = 0;
	for (int k = kpos_ix + 1; k <= *nkx; k++) {
		iy = krutx[k];
		krutx[k] = i0;
		i0 = iy;
	} 
	(*nkx)++;
	krutx[*nkx] = 0;
	
	*kdx = 0;
	*ktx = 0;
	for (int k = 1; k <= (*nkx)-1; k++) {
		*kdx += d[krutx[k]][krutx[k+1]];
		*ktx += t[krutx[k]][krutx[k+1]] + delta[krutx[k+1]];
	}	
}

void feask(int *krutx, int nkx, int ktx, int ix, int *kpos_ix, int *extra_d, int *extra_t) {
	/* trova la migliore posizione kpos_ix per inserire ix in krutx
	 * kpos_ix = -1 non esiste posizione ammissibile
	 */
	int i1,i2;
	int plus_d,plus_t;
	*kpos_ix = -1;
	*extra_d = 100000;
	*extra_t = 100000;
	for (int k = 1; k <= nkx-1; k++) {
		i1 = krutx[k];
		i2 = krutx[k+1];
		plus_d = d[i1][ix] + d[ix][i2] - d[i1][i2];
		plus_t = t[i1][ix] + t[ix][i2] - t[i1][i2] + delta[ix];
		if (ktx + plus_t <= W) {
			if (plus_d < *extra_d) {
				*extra_d = plus_d;
				*extra_t = plus_t;
				*kpos_ix = k;
			}
		}
	}
}

void remove_customer(int *krutx, int nkx, int ix, int *kruty, int *nky, int *kdy, int *kty) {
	/* toglie il cliente ix da krutx e scrive il risultata in kruty di cui calcola distanza kdy e tempo kty*/
	int i;
	int i0 = 0;
	*kdy = 0;
	*kty = 0;
	kruty[1] = 0;
	*nky = 1;

	for (int k = 2; k <= nkx; k++) {
		i = krutx[k];
		if (i != ix) {
			(*nky)++;
			kruty[*nky] = i;
			*kdy += d[i0][i];
			*kty += t[i0][i] + delta[i];
			i0 = i;
		}
	}
}

void printResult() {
	//printf("%d\n", route[1][4]);
	//printf("%d\n", route[6][2]);
	for (int count = 1; count <= nroute; count++) {
		printf("ROUTE %d con %d elementi\n", count, nk[count]);
		for (int i = 1; i <= nk[count]; i++) {
			printf("%d,", route[count][i]);
		}
		printf("\nDURATA    : %d\n", durata[count]);
		printf("DISTANZA  : %d\n", distanza[count]);
		printf("\n");
	}
	printf("nresto: %d\n",nresto);

	//printResultInFile();
}

void printResultInFile() {

	if (remove(outPath) == 0) 
      	printf("Deleted successfully\n"); 
    else
    	printf("Unable to delete the file\n"); 

    FILE *out;
    char *line = "";
  	char s[2000];
  	int distTot = 0;
  	int timeTot = 0;

  	out = fopen(outPath, "w");
  	for (int count = 1; count <= nroute; count++) {
  		line= "";
  		sprintf(s,"Viaggio  %d,   Distanza  %d,   Tempo  %d\n", count, distanza[count], durata[count]);
  		line = concat(line, s);
  		fprintf(out,"%s\n", line);
  		distTot += distanza[count];
  		timeTot += durata[count];

  		line= "";
  		sprintf(s,"               cliente Localita             dist. time priorita\n");
  		line = concat(line, s);
  		fprintf(out,"%s\n", line);
		for (int i = 1; i <= nk[count]; i++) {
			line = "";
  			sprintf(s,"            %d    %d    %s       %d     %d      %d\n", i,route[count][i],loc[route[count][i]],0,0,pigreco[route[count][i]]);
  			line = concat(line, s);
  			fprintf(out,"%s\n", line);
			//printf("%d,", route[count][i]);
		}
	}

	line= "";
  	sprintf(s,"\n\n\nDistanza Totale percorsa  :  %d\nTempo Totale impiegato  :  %d", distTot, timeTot);
  	line = concat(line, s);
  	fprintf(out,"%s\n", line);
}

char* concat(const char *s1, const char *s2) {
  char *result = malloc(strlen(s1) + strlen(s2) + 1); // +1 for the null-terminator
  // in real code you would check for errors in malloc here
  strcpy(result, s1);
  strcat(result, s2);
  return result;
}




