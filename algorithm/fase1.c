#ifndef __FASE_1__
#define __FASE_1__
#include "fase1.h"
#include "var_global.h"


void fase1() {
	int sav[n][n];
	initSaving();
	/* agenti è una word di 64 bit dove il bit k = 1 se l'agente k è libero e =0 altrimenti
	 * per inizializzare agenti si usa lbit(k) così come definito nel Progrma Main
	 */
	agenti = 0;
	int k;
	for (k = 1; k <= m; k++) {
		agenti = agenti | lbit[k];
	}

	/* nresto è il numero di clienti da servire
	 * nroute p il numero di route che verranno prodotte
	 * deg[i] è un fleg associato ad ognli cliente i (=0 se i non è stato servito =1 altrimenti)
	*/
	nresto = 0;
	int i;
	for (i = 1; i <= n; i++) {
		deg[i] = 0;
		nresto++;
	}
	nroute = 0;

	if (nresto == 0 || nresto == m) {
		//stop Fase 1
	} else {
		initNewRouteStep1();
		if (!boolGotoFase2) {
			espandiStep2();
		}
	}
}

void initSaving() {

	/* calcola la matrice saving sav(i,j), i ∈ V' e j ∈ V' che sono compatibili, ovvero per cui esiste almeno un agente che li può servire.
	 * Se i e j sono incompatibili si pone sav(i,j) = -100.
	 * Nel saving si considera la priorità per cui detto ss il saving per la coppia di clienti i,j si definisce:
	 * 	- sav(i,j) = 200.000 + ss quando 𝜋(i) ≤ 2 E 𝜋(j) ≤ 2
	 *  - sav(i,j) = 100.000 + ss quando 𝜋(i) ≤ 2 E 𝜋(j) > 2 OPPURE 𝜋(i) > 2 E 𝜋(j) ≤ 2
	 *  - sav(i,j) =           ss quando 𝜋(i) > 2 E 𝜋(j) > 2         
	 */
	int ss,i,j;
	for (i = 1; i <= n; i++) {
		for (j = i; j <= n; j++) {
			if (i == j) {
				ss = -1;
				if (pigreco[i] <= 2) {
					sav[i][j] = BOOTH_MINOR_2 + ss;
				} else {
					sav[i][j] = ss;
				}
			} else if ((S[i] & S[j]) == 0) {
				sav[i][j] = -100;
			} else {
				ss = d[0][i] + d[0][j] - d[i][j];
				if (pigreco[i] <= 2 && pigreco[j] <= 2) {
					sav[i][j] = BOOTH_MINOR_2 + ss;
				} else if (pigreco[i] <= 2 || pigreco[j] <= 2) {
					sav[i][j] = SINGLE_MINOR_2 + ss;
				} else {
					sav[i][j] = ss;
				}
			}
			sav[j][i] = sav[i][j];
			printf("SAV   :    %d\n", sav[i][j]);
		}
	}
}


void initNewRouteStep1() {
	int max_sav = -100;
	int i_star = 0;
	int j_star = 0;
	int i,j;

	boolGotoFase2 = FALSE0;

	for (i = 1; i <= n; i++) {
		for (int j = 1; j <= n; j++) {
			if (sav[i][j] > max_sav) {
				if (deg[i] + deg[j] == 0 && (S[i] & S[j] & agenti) > 0) {
					max_sav = sav[i][j];
					i_star = i;
					j_star = j;
				}
			}
		}
	}

	/* si assume i_star = j_star = 0 se una detta coppia non esiste. In questo caso è finita la Fase 1.
	 * Se i_star = j_star = 0 e nresto > 0 allora ciò implica che gli agenti ancora disponibili non sono compatibili
	 * con i clienti ancora non serviti.
	 * Se i_star = j_star > 0 allora il cliente i_star non può essere accoppiato con nessuno e quindi va costruita una 
	 * route che contiene solo i_star.
	 */

	if (i_star == 0 && j_star == 0) {
		boolGotoFase2 = TRUE1;
		return;
	} else {
		/* Inizializza il nuovo viaggio nroute+1 */
		nroute++;
		/* ag è la maschera-a-bit degli agenti che possono servire sia i_star che j_star */
		ag = S[i_star] & S[j_star] & agenti;

		if (i_star == j_star) {
			deg[i_star] = 1;
			nk[nroute] = 3;
			route[nroute][1] = 0;
			route[nroute][2] = i_star;
			route[nroute][3] = 0;
			/* Assegnare il viaggio nroute al primo agente k_star il cui bit di ag vale 1 */
			int k_star = getK_star();
			agente[nroute] = k_star;
			/* poni a 0 il bit k_star di agenti (xor)*/
			ag = ag^lbit[k_star];

			durata[nroute] = t[0][i_star] + delta[i_star] + t[i_star][0];
			distanza[nroute] = d[0][i_star] + d[i_star][0];
			nresto--;
			/*oppure goto.......*/
			initNewRouteStep1();
		} else {
			deg[i_star] = 1;
			deg[j_star] = 1;
			nk[nroute] = 4;
			route[nroute][1] = 0;
			route[nroute][2] = i_star;
			route[nroute][3] = j_star;
			route[nroute][4] = 0;
			durata[nroute] = t[0][i_star] + delta[i_star] + t[i_star][j_star] + delta[j_star] + t[j_star][0];
			distanza[nroute] = d[0][j_star] + d[i_star][j_star] + d[j_star][0];
			nresto -= 2;
		}	
	}

	printf("BOOL    :   %d\n", boolGotoFase2);
	printf("NRoute  :   %d\n", nroute);
	printf("nresto  :   %d\n", nresto);
}

void espandiStep2() {
	/* trova il cliente i_star avente saving massimo con il cliente i_primo = route[nroute][2] e il cliente j_star avente
	 * il saving massimo con il cliente j_primo = route[nroute][nk[route]-1]
	 */
	int i_primo = route[nroute][2];
	int i_star = 0;
	int i_star_sav = -100;
	int i_star_time_extra = 0;
	int i_star_dist_extra = 0;
	int i, extra, kd;

	int j_primo = route[nroute][nk[nroute]-1];
	int j_star = 0;
	int j_star_sav = -100;
	int j_star_time_extra = 0;
	int j_star_dist_extra = 0;
	int j;

	int k, k_star;
	int lt,ld,i1,i_temp,ii;

	/* Sia i_star il cliente per cui 
	 *		sav(i_star, i_primo) = max{sav(i,i_primo) : i ∈ V, tali che deg(i) = 0 AND (S(i) & ag) > 0 AND il viaggio che
	 *                             si ottiene inserendo il cliente i prima di i_primo soddisfa i vincoli}.
	 * Si pone i_star_sav = sav(i_star, i_primo)
	 * Si assume i_star = 0 e i_star_sav = -100 se un tale cliente non esiste.
	 */
	for (i = 1; i <= n; i++) {
		if (deg[i] == 0 && (S[i] & ag) > 0) {
			extra = t[0][i] + t[i][i_primo] - t[0][i_primo];
			kd = durata[nroute] + extra;
			if (sav[i][i_primo] > i_star_sav && kd < W) {
				i_star = i;
				i_star_time_extra = extra;
				i_star_dist_extra = d[0][i] + d[i][i_primo] - d[0][i_primo];
				i_star_sav = sav[i][i_primo];
			}
		}
	}

	/* Sia j_star il cliente per cui 
	 *		sav(j_star, j_primo) = max{sav(j,j_primo) : j ∈ V, tali che deg(j) = 0 AND (S(j) & ag) > 0 AND il viaggio che
	 *                             si ottiene inserendo il cliente j dopo j_primo soddisfa i vincoli}.
	 * Si pone j_star_sav = sav(j_star, j_primo)
	 * Si assume j_star = 0 e j_star_sav = -100 se un tale cliente non esiste.
	 */
	for (j = 1; j <= n; j++) {
		if (deg[j] == 0 && (S[j] & ag) > 0) {
			extra = t[0][j] + t[j][j_primo] + delta[j] - t[0][j_primo];
			kd = durata[nroute] + extra;
			if (sav[j][j_primo] > j_star_sav && kd < W) {
				j_star = j;
				j_star_time_extra = extra;
				j_star_dist_extra = d[0][j] + d[j][j_primo] - d[0][j_primo];
				j_star_sav = sav[j][j_primo];
			}
		}
	}

	/* se i_star == 0 e anche j_star == 0 allora il corrente viaggio nroute non può essere espanso ulteriormente */
	if (i_star == 0 && j_star == 0) {
		/* La corrente route non può essere espansa ulteriormente
		 * Assegna la route nroute al primo agente k_star il cui bit di ag che vale 1 (SI PUO' FARE DI MEGLIO PER SCEGLERE k_star)
		 */
		int k_star = getK_star();
		agente[nroute] = k_star;
		/* porre a 0 il bit k_star di agenti (xor)*/
		agenti = agenti^lbit[k_star];

		//ricalcola la durata del viaggio nroute
		lt = 0;
		ld = 0;
		i1 = 0;
		for (k = 2; k <= nk[nroute]; k++) {
			j = route[nroute][k];
			lt += t[i1][j] + delta[j];
			ld += d[i1][j];
			i1 = j;
		} 
		durata[nroute] = lt;
		distanza[nroute] = ld;
		//go to step 1;
		initNewRouteStep1();
	} else {
		if (i_star_sav > j_star_sav) {
			// inserisci i_star prima di i_primo = route(nroute,2)
			i_temp = i_star;
			for (k = 2; k <= nk[nroute]; k++) {
				ii = route[nroute][k];
				route[nroute][k] = i_temp;
				i_temp = ii;
			}
			nk[nroute]++;
			route[nroute][nk[nroute]] = 0;
			durata[nroute] += i_star_time_extra;
			distanza[nroute] += i_star_dist_extra;
			ag = ag & S[i_star];
		} else {
			//inserisci j_star dopo route(nroute, nk(nroute)-1)
			route[nroute][nk[nroute]] = j_star;
			nk[nroute]++;
			route[nroute][nk[nroute]] = 0;
			durata[nroute] += j_star_time_extra;
			distanza[nroute] += j_star_dist_extra;
			ag = ag & S[j_star];
		}
		nresto--;

		if (nk[nroute] >= 6) {
			//ottimizza il corrente viaggio nroute mediante il 2 ottimale 
		}
		espandiStep2();
	}
}

int getK_star() {
	int k_star;
	for (k_star = 0; k_star < m; k_star++) {
		if ((ag & lbit[k_star]) != 0) {
			return k_star;
		}
	}
	return FALSE0;
}

#endif __FASE_1__
