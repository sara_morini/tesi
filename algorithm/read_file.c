#include "read_file.h"

void readFile(char problem_file[25], char dist_file[25], char time_file[25]) {
	FILE *prob;
	FILE *dist;
	FILE *time;

	char line[800];
	char *token;

	prob = fopen(problem_file, "r");
	dist = fopen(dist_file, "r");
	time = fopen(time_file, "r");

	/*PROBLEMA*/
	fgets(line, sizeof(line), prob);
	token = strtok(line, ",");
	n = atoi(token);
	printf("%d\n", n);
	token = strtok(NULL,",");
	m = atoi(token);
	printf("%d\n", m);
	token = strtok(NULL,",");
	W = atoi(token);
	printf("%d\n", W);

	if (n > N_MAX || m > M_MAX) {
		printf("ERRORE\n");
	} else {
		int i,j;
		for (i = 1; i <= n; i++) {
			fgets(line, sizeof(line), prob);
			int c;
			
			token = strtok(line, ","); 	//#cliente
			token = strtok(NULL,",");  	//latitudine
			token = strtok(NULL,",");	//longitudine
			token = strtok(NULL,",");	//nome
			token = strtok(NULL,",");	//priorità

			pigreco[i] = atoi(token);
			token = strtok(NULL,",");		//tempi servizio
			delta[i] = atoi(token);
			token = strtok(NULL,",");		//capacità
			S[i] = atoi(token);
			printf("%d\n", i);
			printf("pigreco: %d\n", pigreco[i]);
			printf("delta: %d\n", delta[i]);
			printf("S: %ld\n", S[i]);

		}
		fclose(prob);
		

		/*DISTANZE*/
		for (i = 0; i <= n; i++) {
			fgets(line, sizeof(line), dist);
			token = strtok(line, ",");
			for (j = 0; j <= n; j++) {
				d[i][j] = atoi(token);
				token = strtok(NULL, ",");
				printf("%d,%d : %d\n",i,j,d[i][j]);
			}
		}
		fclose(dist);


		/*TEMPI*/
		for (i = 0; i <= n; i++) {
			fgets(line, sizeof(line), time);
			token = strtok(line, ",");
			for (j = 0; j <= n; j++) {
				t[i][j] = atoi(token);
				token = strtok(NULL, ",");
			}
		}
		fclose(time);
	}

}
