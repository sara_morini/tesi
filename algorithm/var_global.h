#ifndef __VAR_GLOBAL__
#define __VAR_GLOBAL__

#define BOOTH_MINOR_2 200000
#define SINGLE_MINOR_2 100000
#define N_MAX 201
#define M_MAX 61
#define MAX_ROUTE 61
#define N_MAX_VIAGGIO 22
#define FALSE0 0
#define TRUE1 1

/*numero clienti n <= 200*/
int n = 0;
/*numero agenti si assume m<=60*/
int m; 
/*tempo massimo di lavoro di ogni agente m*/
int W; 
/*priorità*/
int pigreco[N_MAX];
/*tempo di servizio di ogni cliente*/
int delta[N_MAX]; 
/*S(0), S(1) maschera di m bit dove il bit k = 1 se l'agente k può servire il cliente i =0 altrimenti (i indice della maschera)....???????*/
long int S[N_MAX]; 

/*matrice distanze i=0..n j=0..n*/
int d[N_MAX+1][N_MAX+1];
/*matrice tempi i=0..n j=0..n*/
int t[N_MAX+1][N_MAX+1];

/*matrice saving i=1..n j=1..n NON UTILIZZARE LO ZERO*/
int sav[N_MAX+1][N_MAX+1];
/*???????????*/
long int lbit[M_MAX+1];
/*maschera degli agenti ancora disponibili*/
long int agenti; 
/* maschera degli agenti ancora disponibili per la route corrente. 
 * Prima di costruire una nuova route ag=agenti ma man mano che un cliente i viene aggiunto alla route emergente allora ag = agenti ∩ S(i)
 */
long int ag;
/*Numero di viaggi generati massimo m*/
int nroute;
int nresto;
/*deg[i] è un fleg associato ad ogni cliente i (=0 se i non è stato servito =1 altrimenti)*/
int deg[N_MAX];

/*nk(k) numero di clienti + 2 della route k=1..nroute*/
int nk[MAX_ROUTE];
/*matrice dei viaggi generati. Es. se la route k visita in sequenza i clienti 23,11,14 allora 
 *nk(k) = 5
 *route(k,1) = 0
 *route(k,2) = 23
 *route(k,3) = 11
 *route(k,4) = 14
 *route(k,5) = 0
 */
int route[M_MAX][N_MAX_VIAGGIO];
/*durata(k) durata del viaggio k*/
int durata[MAX_ROUTE];
/*distanza(k) distanza del viaggio k*/
int distanza[MAX_ROUTE];
/*agente(k) agente assegnato al viaggio k*/
int agente[MAX_ROUTE];

int boolGotoFase2;

#endif