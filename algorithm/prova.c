#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include <errno.h>

void func(int *p, int *c) {
    printf("%p\n", p);
    printf("%d\n", p[1]);
    p[1] = 10;
    int k = *c - 10;
    printf("k   %d\n", k);
}

int main() {
    int p[3][2];
    int c = 101;
    p[0][0] = 1;
    p[0][1] = 4;
    p[1][0] = 2;
    p[1][1] = 5;
    p[2][0] = 3;
    p[2][1] = 6; 
    func(p[0],&c);
    printf("%d\n", p[0][1]);
    printf("c : %d\n", c);
}