#ifndef __ROUTINE_OPT2__
#define __ROUTINE_OPT2__

#include "routine_opt2.h"
#include "var_global.h"

int improve;

int do_opt_2() {
	int kdist_opt2 = 0;
	int la,nka,k;
	int kruta[N_MAX_VIAGGIO];
	int kda,kta;

	for (la = 1; la <= nroute; la++) {
		nka = nk[la];
		for (k = 1; k <= nka; k++) {
			kruta[k] = route[la][k];
		}
		kda = distanza[la];
		kta = durata[la];

		opt_2(kruta,nka,kda,kta);

		kdist_opt2 += kda

		if (improve > 0) {
			for (k = 1; k <= nka; k++) {
				route[la][k] = kruta[k];
			}
			distanza[la] = kda;
			durata[la] = kta;
		}

	}

	return kdist_opt2;

}

void opt_2(int krut[], int nkrut, int kd, int kt) {
	int knew[n+1];
	int x1,x2,y1,y2,;
	int k,la,lb,lk,jk;

	improve = 0;

cento:for (la = 2; la <= nkrut-2; la++) {
		x1 = krut[la-1];
		y1 = krut[la];
		
		for (lb = la+2; lb <= nkrut; lb++) {
			x2 = krut[lb-1];
			y2 = krut[lb];

			if (d[x1][x2] + d[y1][y2] < d[x1][y1] + d[x2][y2]) {
				improve++;

				for (lk = 1; lk <= la-1; lk++) {
					knew[lk] = krut[lk];
				}

				jk = la - 1;

				for (lk = lb-1; lk <= la-1; lk++) {
					jk++;
					knew[jk] = krut[lk];
				}

				for (k = lb; k <= nkrut; k++) {
					knew[k] = krut[k];
				}

				for (k = 1; k <= nkrut; k++) {
					krut[k] = knew[k];
				}
				goto cento;
			}
		}
	}
	return;
}

#endif __ROUTINE_OPT2__
