
clear all
close all

coordinate=[
42.5311 12.5196;
42.4978 12.4767;
42.5517 12.6328;
42.8537 13.5603;
42.8551 13.5883;
43.0570 13.8489;
43.6075 13.4978;
43.7356 12.8347;
43.2102 13.0572;
43.2055 13.0557;
43.0794 13.4888;
43.2932 13.4554;
42.8052 13.9388;
42.7832 13.9514;
42.5163 14.1512;
42.4687 14.2040;
42.4443 14.2071;
42.8160 13.8554;
42.8032 13.9411;
42.9510 13.8832;
42.9543 13.8821]

lables_coordinate = [
43.0794 13.4888;
43.6075 13.4978;
43.2055 13.0557;
42.4687 14.2040;
42.9543 13.8821;
42.5517 12.6328;
42.8052 13.9388;
42.5311 12.5196;
42.4978 12.4767;
42.4443 14.2071;
42.5163 14.1512;
43.2932 13.4554;
42.8537 13.5603;
42.8551 13.5883;
43.2102 13.0572;
42.8032 13.9411;
42.9510 13.8832;
42.7832 13.9514;
43.7356 12.8347;
43.0570 13.8489;
42.8160 13.8554]

per_agente=[6;4;2;5;4];

deposito=[43.0561 13.7011]; 
    figure
    hold on
inizio=1;
fine = 21;
n_agenti = 5;
labels = cellstr(num2str([inizio:fine]'));

text(lables_coordinate(inizio:fine,2)+0.02,lables_coordinate(inizio:fine,1),labels)
for i=1:n_agenti
    plot(coordinate(inizio:inizio+per_agente(i)-1,2),coordinate(inizio:inizio+per_agente(i)-1,1),'-o')
    inizio=inizio+per_agente(i);
end

labels = cellstr(num2str([0]'));
plot(deposito(2),deposito(1),'sk')
text(deposito(2)+0.02,deposito(1),labels)



