#include <math.h>
/* Questa funzione calcola la distanza tra due punti 
sulla superficie terrestre, date le coordinate in
latitudine e longitudine espresse in
gradi decimali */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

double disgeod(double latA, double lonA, 
              double latB, double lonB);
void readFile(char problem_file[25], char path[25], char date[25]);
void createFile(int **dist, int **time, char path[25], int n, char date[25]);
char* concat(const char *s1, const char *s2);
char* replace_char(char* str, char find, char replace);
/*
 * argv[1] = problem_file
 * argv[2] = partial_path
 * argv[3] = date
 */
int main (int argc, char *argv[])
{

      printf("MAIN\n");
      readFile(argv[1], argv[2], argv[3]);
      double distanza =  disgeod(43.0561,13.7011,43.0794,13.4888) * 1.2;
      double tempo = distanza/50;
      printf("\n La distanza fra A e B e' : %f  km\n", distanza);
      printf("\n Il tempo fra A e B e' : %f  km\n", tempo);
}

double disgeod (double latA, double lonA,
                double latB, double lonB)
{
      /* Definisce le costanti e le variabili */
      const double R = 6371;
      const double pigreco = 3.1415927;
      double lat_alfa, lat_beta;
      double lon_alfa, lon_beta;
      double fi;
      double p, d;

      if (latA == latB && lonA == lonB) {
        return 0.0;
      } else {
        /* Converte i gradi in radianti */
        lat_alfa = pigreco * latA / 180;
        lat_beta = pigreco * latB / 180;
        lon_alfa = pigreco * lonA / 180;
        lon_beta = pigreco * lonB / 180;
        /* Calcola l'angolo compreso fi */
        fi = fabs(lon_alfa - lon_beta);
        /* Calcola il terzo lato del triangolo sferico */
          p = acos(sin(lat_beta) * sin(lat_alfa) + 
          cos(lat_beta) * cos(lat_alfa) * cos(fi));
        /* Calcola la distanza sulla superficie 
        terrestre R = ~6371 km */
        d = p * R;
        return(d);
      }
}


void readFile(char problem_file[25], char path[25], char date[25]) {
  FILE *prob1;
  FILE *prob2;

  char line[800];
  char *token;

  double latitudineP,longitudineP;
  double latitudineA,longitudineA;

  int **dist;
  int **time;

  int n,i,j;

  prob1 = fopen(problem_file, "r");
  /*PROBLEMA*/
  fgets(line, sizeof(line), prob1);
  token = strtok(line,";"); //n
  n = atoi(token);
  printf("N : %d\n", n);
  token = strtok(NULL,";");  //m
  //m = atoi(token);
  token = strtok(NULL,";");  //W
  //W = atoi(token);

  dist = (int**) malloc ((n+1) * sizeof(int*));
  time = (int**) malloc ((n+1) * sizeof(int*));
  for (i = 0; i <= n; i++) {
    dist[i] = (int*) malloc ((n+1) * sizeof(int));
    time[i] = (int*) malloc ((n+1) * sizeof(int));
  }
  printf("malloc\n");
  
  for (i = 0; i <= n; i++) {
    fgets(line, sizeof(line), prob1);
    int c;
      
    token = strtok(line,";");  //#cliente
    token = strtok(NULL,";");   //latitudine
    latitudineP = atof(replace_char(token,',','.'));
    token = strtok(NULL,";"); //longitudine
    longitudineP = atof(replace_char(token,',','.'));
    prob2 = fopen(problem_file, "r");
    fgets(line, sizeof(line), prob2);
    printf("for\n");
    for (j = 0; j <= n; j++) {
      fgets(line, sizeof(line), prob2);
      token = strtok(line,";");  //#cliente
      token = strtok(NULL,";");   //latitudine
      latitudineA = atof(replace_char(token,',','.'));
      token = strtok(NULL,";"); //longitudine
      longitudineA = atof(replace_char(token,',','.'));

      int distanza =  ((disgeod(latitudineP, longitudineP, latitudineA, longitudineA)) * 10 + 0.5) * 1.2;
      int tempo;
      if (distanza/10 <= 50) {
        tempo =  distanza*60/600;
      } else if (distanza/10 <= 100) {
        tempo = distanza*60/800;
      } else {
        tempo = distanza*60/1100;
      }

      dist[i][j] = distanza;
      dist[j][i] = distanza;

      time[i][j] = tempo;
      time[j][i] = tempo;
      //printf("%d , %d\n",i,j);
    }
  }

  fclose(prob1);
  fclose(prob2);

  createFile(dist, time, path, n, date);
}


void createFile(int **dist, int **time, char path[25], int n, char date[25]) {
  FILE *distanze;
  FILE *tempi;

  int i,j;
  char *line = "";
  char s[200];

  char *distPath = concat(concat(concat(path,"dist-"),date),".dat");
  char *timePath = concat(concat(concat(path,"time-"),date),".dat");
  distanze = fopen(distPath, "w");
  tempi = fopen(timePath, "w");



  for (i = 0; i <= n; i++) {
    for (j = 0; j <= n; j++) {
      sprintf(s,"%d", dist[i][j]);
      line = concat(line, s);
      if (j != n) {
        line = concat(line,";");
      }
    }
    fprintf(distanze, "%s\n", line);
    line = "";
  }


  line = "";

  for (i = 0; i <= n; i++) {
    for (j = 0; j <= n; j++) {
      sprintf(s,"%d", time[i][j]);
      line = concat(line, s);
      if (j != n) {
        line = concat(line,";");
      }
    }
    fprintf(tempi, "%s\n", line);
    line = "";
  }

}


char* concat(const char *s1, const char *s2) {
  char *result = malloc(strlen(s1) + strlen(s2) + 1); // +1 for the null-terminator
  // in real code you would check for errors in malloc here
  strcpy(result, s1);
  strcat(result, s2);
  return result;
}

char* replace_char(char* str, char find, char replace) {
    char *current_pos = strchr(str,find);
    while (current_pos){
        *current_pos = replace;
        current_pos = strchr(current_pos,find);
    }
    return str;
}

